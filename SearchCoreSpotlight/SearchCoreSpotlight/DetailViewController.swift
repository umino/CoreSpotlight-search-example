//
//  DetailViewController.swift
//  SearchCoreSpotlight
//
//  Created by phuongnl on 5/8/18.
//  Copyright © 2018 phuongnl. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    @IBOutlet weak var artistAvatar: UIImageView!
    @IBOutlet weak var artistTitle: UILabel!
    @IBOutlet weak var artistDescription: UILabel!
    var artist: Artist!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        
        artistTitle.text = artist.title;
        if artist.imageUrl != nil {
            artistAvatar.sd_setImage(with: URL(string: artist.imageUrl!))
        }
        artistDescription.text = artist.description ?? ""
    }
    
    func setUI() {
        artistAvatar.clipsToBounds = true
        artistAvatar.layer.cornerRadius = artistAvatar.frame.width/2
        artistAvatar.layer.borderWidth = 1
        artistAvatar.layer.borderColor = UIColor.white.cgColor
    }
}
