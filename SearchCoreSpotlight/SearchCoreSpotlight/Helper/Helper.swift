//
//  Helper.swift
//  SearchCoreSpotlight
//
//  Created by phuongnl on 5/8/18.
//  Copyright © 2018 phuongnl. All rights reserved.
//

import Foundation
import UIKit

class Helper {
    static func getListItemFromLocalFile() -> [Artist]? {
        var artists: [Artist]?
        
        if let filePath = Bundle.main.path(forResource: "data", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: filePath), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let jsonResult = jsonResult as? [String: AnyObject]{
                    if let artistArray = jsonResult["items"] as? [[String: AnyObject]] {
                        artists = [Artist]()
                        
                        for a in artistArray {
                            let a = Artist(dictionary: a)
                            artists?.append(a)
                        }
                        return artists!
                    }
                }
            } catch {
                // handle error
            }
        }
        return nil
    }
}
