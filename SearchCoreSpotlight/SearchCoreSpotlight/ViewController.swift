//
//  ViewController.swift
//  SearchCoreSpotlight
//
//  Created by phuongnl on 5/8/18.
//  Copyright © 2018 phuongnl. All rights reserved.
//

import UIKit
import CoreSpotlight

class ViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var artistList: [Artist]!
    var searchableList = [CSSearchableItem]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        artistList = Helper.getListItemFromLocalFile() ?? []
        setSearchableList()
        
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.reloadData()
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return artistList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? Cell else {
            fatalError("The dequeued cell is not an instance of Cell.")
        }
        let artist = artistList[indexPath.row]
        cell.set(for: artist)
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        guard let vc = segue.destination as? DetailViewController,
            let indexPath = tableView.indexPathForSelectedRow else {
            return
        }
        vc.artist = artistList[indexPath.row]
    }
}

extension ViewController {
    func setSearchableList() {
        // remove dupplicate search items
        CSSearchableIndex.default().deleteAllSearchableItems { (error) in
            if error != nil {
                print(error?.localizedDescription ?? "cannot clear CSSearchableIndex.default")
            }
        }
        // add search items
        for artist in artistList {
            searchableList.append(artist.searchItem)
        }
        CSSearchableIndex.default().indexSearchableItems(searchableList) { (error) in
            if error != nil {
                print(error?.localizedDescription ?? "error")
            }else{
                print("items indexed witch success!")
            }
        }
    }
}
