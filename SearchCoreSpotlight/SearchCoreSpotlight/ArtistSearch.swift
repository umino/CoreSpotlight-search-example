//
//  ArtistSearch.swift
//  SearchCoreSpotlight
//
//  Created by phuongnl on 5/8/18.
//  Copyright © 2018 phuongnl. All rights reserved.
//

import Foundation
import CoreSpotlight
import MobileCoreServices
import SDWebImage

extension Artist {
    public static let domainId = "com.phuongnl.SearchCoreSpotlight.artist"
    
    public var userActivityinfo: [String: Int] {
        return ["id": id]
    }
    
//    public var userActivity: NSUserActivity {
//        let activity = NSUserActivity(activityType: Artist.domainId)
//        activity.title = title
//        activity.userInfo = userActivityinfo
//        activity.isEligibleForSearch = true
//        activity.contentAttributeSet = attributeSet
//        return activity
//    }
    
    public var searchItem: CSSearchableItem {
        return CSSearchableItem(uniqueIdentifier: String(id), domainIdentifier: Artist.domainId, attributeSet: attributeSet)
    }
    
    public var attributeSet: CSSearchableItemAttributeSet {
        let attributeSet = CSSearchableItemAttributeSet(itemContentType: kUTTypeContact as String)
        attributeSet.title = title
        attributeSet.contentDescription = description
        if let thumbnailURL = imageUrl,
            let path = SDImageCache.shared().defaultCachePath(forKey: thumbnailURL),
            let url = URL(string: "file://\(path)") {
            attributeSet.thumbnailData = try? Data(contentsOf: url)
        }
        attributeSet.keywords = title.components(separatedBy: " ")
        return attributeSet
    }
}
