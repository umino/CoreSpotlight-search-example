//
//  Item.swift
//  SearchCoreSpotlight
//
//  Created by phuongnl on 5/8/18.
//  Copyright © 2018 phuongnl. All rights reserved.
//

import Foundation

class Artist {
    var id: Int = 0
    var title: String!
    var imageUrl: String?
    var description: String?
    
    init(dictionary: [String: AnyObject]) {
        guard let id = dictionary["id"] as? Int,
            let title = dictionary["title"] as? String else {
            return
        }
        self.id = id
        self.title = title
        self.imageUrl = dictionary["image"] as? String
        self.description = dictionary["description"] as? String
    }
}
