//
//  CellTableViewCell.swift
//  SearchCoreSpotlight
//
//  Created by phuongnl on 5/8/18.
//  Copyright © 2018 phuongnl. All rights reserved.
//

import UIKit
import SDWebImage

class Cell: UITableViewCell {
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellTitle: UILabel!
    @IBOutlet weak var cellDescription: UILabel!
    var artist: Artist!
    
    func set(for artist: Artist) {
        setUI()
        
        cellTitle.text = artist.title
        cellDescription.text = artist.description?.components(separatedBy: ".")[0]
        if artist.imageUrl != nil {
            cellImage.sd_setImage(with: URL(string: artist.imageUrl!))
        }
        self.artist = artist
    }
    
    func setUI() {
        cellImage.clipsToBounds = true
        cellImage.layer.cornerRadius = cellImage.frame.width/2
        cellImage.layer.borderWidth = 1
        cellImage.layer.borderColor = UIColor.white.cgColor
    }
}
